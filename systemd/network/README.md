Currently i'm bind-mounting this dir, but it needs automation:

# TODO's:

- [x] All generated files need to be placed under `$builddir`.
- [x] The network config files from `$builddir` should be installed into:<br>
```
$(DESTDIR)$(sysconfdir)/$(SystemdNetworkedNS)
```
- [x] **Before build.**<br>
Because `systemd-analyze` needs to be able to find all "pre-dot" files and dirs,
from a directory that is mentioned by `systemd-path systemd-search-network`,
we need to **bind-mount** this source-dir.<br>
```
systemd-mount -o bind $PWD /run/systemd/network
```
- [x] **At end of everything**<br>
We need to un-mount this source-dir again, to leave the system in a clean state:<br>
```
systemd-umount /run/systemd/network
```