#!/usr/bin/env bash
#	v1.0 <description>.
#
#copyrights

function gen_info(){
	cmd="sudo lspci -vvvnnks ${dev##*/}"
	printf "%s\n" \
		"=====" \
		"$> $cmd" \
		"====="
	$cmd

	modes=('test' 'info' 'info -a')
	for mode in "${modes[@]}"; do
		cmd="sudo udevadm ${mode} ${dev}"
		printf "%s\n" \
			"=====" \
			"$> $cmd" \
			"====="
		$cmd
	done
}

dev=/sys/devices/pci0000:00/0000:00:19.0
gen_info >&debug.eth0.txt

dev=/sys/devices/pci0000:00/0000:00:1c.3/0000:05:00.0
gen_info  >&debug.wifi.txt
#
# Editor modelines  -  https://www.wireshark.org/tools/modelines.html
#
# Local variables:
# c-basic-offset: 4
# tab-width: 4
# indent-tabs-mode: t
# End:
#
# vi: set shiftwidth=4 tabstop=4 noexpandtab:
# :indentSize=4:tabSize=4:noTabs=false:
#
