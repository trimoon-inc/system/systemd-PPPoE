=====
$> sudo lspci -vvvnnks 0000:00:19.0
=====
00:19.0 Ethernet controller [0200]: Intel Corporation Ethernet Connection (2) I218-V [8086:15a1] (rev 05)
	Subsystem: ASUSTeK Computer Inc. Ethernet Connection (2) I218-V [1043:85c4]
	Control: I/O- Mem+ BusMaster+ SpecCycle- MemWINV- VGASnoop- ParErr- Stepping- SERR- FastB2B- DisINTx+
	Status: Cap+ 66MHz- UDF- FastB2B- ParErr- DEVSEL=fast >TAbort- <TAbort- <MAbort- >SERR- <PERR- INTx-
	Latency: 0
	Interrupt: pin A routed to IRQ 32
	NUMA node: 0
	Region 0: Memory at fb600000 (32-bit, non-prefetchable) [size=128K]
	Region 1: Memory at fb622000 (32-bit, non-prefetchable) [size=4K]
	Region 2: I/O ports at f000 [disabled] [size=32]
	Capabilities: [c8] Power Management version 2
		Flags: PMEClk- DSI+ D1- D2- AuxCurrent=0mA PME(D0+,D1-,D2-,D3hot+,D3cold+)
		Status: D0 NoSoftRst- PME-Enable- DSel=0 DScale=1 PME-
	Capabilities: [d0] MSI: Enable+ Count=1/1 Maskable- 64bit+
		Address: 0000000000000000  Data: 0000
	Capabilities: [e0] PCI Advanced Features
		AFCap: TP+ FLR+
		AFCtrl: FLR-
		AFStatus: TP-
	Kernel driver in use: e1000e
	Kernel modules: e1000e

=====
$> sudo udevadm test /sys/devices/pci0000:00/0000:00:19.0
=====
calling: test
version 232
=== trie on-disk ===
tool version:          232
file size:         8799916 bytes
header size             80 bytes
strings            1856092 bytes
nodes              6943744 bytes
Load module index
Found container virtualization none
timestamp of '/etc/systemd/network' changed
Parsed configuration file /lib/systemd/network/99-default.link
Parsed configuration file /etc/systemd/network/70-persistent-net.wifi.link
Parsed configuration file /etc/systemd/network/70-persistent-net.utp.link
Created link configuration context.
timestamp of '/etc/udev/rules.d' changed
timestamp of '/lib/udev/rules.d' changed
Reading rules file: /lib/udev/rules.d/39-usbmuxd.rules
Reading rules file: /lib/udev/rules.d/40-usb-media-players.rules
Reading rules file: /lib/udev/rules.d/40-usb_modeswitch.rules
Reading rules file: /lib/udev/rules.d/50-firmware.rules
Reading rules file: /lib/udev/rules.d/50-udev-default.rules
Reading rules file: /lib/udev/rules.d/55-Argyll.rules
/lib/udev/rules.d/55-Argyll.rules:129: IMPORT found builtin 'usb_id --export %p', replacing
Reading rules file: /lib/udev/rules.d/55-dm.rules
Reading rules file: /lib/udev/rules.d/60-block.rules
Reading rules file: /lib/udev/rules.d/60-cdrom_id.rules
Reading rules file: /lib/udev/rules.d/60-crda.rules
Reading rules file: /lib/udev/rules.d/60-drm.rules
Reading rules file: /lib/udev/rules.d/60-evdev.rules
Reading rules file: /lib/udev/rules.d/60-fuse.rules
Reading rules file: /lib/udev/rules.d/60-libgphoto2-6.rules
Reading rules file: /lib/udev/rules.d/60-libsane.rules
Reading rules file: /lib/udev/rules.d/60-nvidia-kernel-common.rules
Reading rules file: /lib/udev/rules.d/60-persistent-alsa.rules
Reading rules file: /lib/udev/rules.d/60-persistent-input.rules
Reading rules file: /lib/udev/rules.d/60-persistent-storage-dm.rules
Reading rules file: /lib/udev/rules.d/60-persistent-storage-tape.rules
Reading rules file: /lib/udev/rules.d/60-persistent-storage.rules
Reading rules file: /lib/udev/rules.d/60-persistent-v4l.rules
Reading rules file: /lib/udev/rules.d/60-serial.rules
Reading rules file: /lib/udev/rules.d/61-gnome-settings-daemon-rfkill.rules
Reading rules file: /lib/udev/rules.d/64-btrfs.rules
Reading rules file: /lib/udev/rules.d/64-xorg-xkb.rules
Reading rules file: /lib/udev/rules.d/65-libwacom.rules
Reading rules file: /lib/udev/rules.d/69-cd-sensors.rules
Reading rules file: /lib/udev/rules.d/69-libmtp.rules
Reading rules file: /lib/udev/rules.d/69-wacom.rules
Reading rules file: /lib/udev/rules.d/70-debian-uaccess.rules
Reading rules file: /lib/udev/rules.d/70-mouse.rules
Reading rules file: /lib/udev/rules.d/70-power-switch.rules
Reading rules file: /lib/udev/rules.d/70-printers.rules
Reading rules file: /lib/udev/rules.d/70-touchpad.rules
Reading rules file: /lib/udev/rules.d/70-uaccess.rules
Reading rules file: /lib/udev/rules.d/71-seat.rules
Reading rules file: /lib/udev/rules.d/73-seat-late.rules
Reading rules file: /lib/udev/rules.d/73-special-net-names.rules
Reading rules file: /lib/udev/rules.d/73-usb-net-by-mac.rules
Reading rules file: /lib/udev/rules.d/75-net-description.rules
Reading rules file: /lib/udev/rules.d/75-probe_mtd.rules
Reading rules file: /lib/udev/rules.d/77-mm-cinterion-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-dell-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-ericsson-mbm.rules
Reading rules file: /lib/udev/rules.d/77-mm-haier-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-huawei-net-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-longcheer-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-mtk-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-nokia-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-pcmcia-device-blacklist.rules
Reading rules file: /lib/udev/rules.d/77-mm-platform-serial-whitelist.rules
Reading rules file: /lib/udev/rules.d/77-mm-qdl-device-blacklist.rules
Reading rules file: /lib/udev/rules.d/77-mm-simtech-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-telit-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-usb-device-blacklist.rules
Reading rules file: /lib/udev/rules.d/77-mm-usb-serial-adapters-greylist.rules
Reading rules file: /lib/udev/rules.d/77-mm-x22x-port-types.rules
Reading rules file: /lib/udev/rules.d/77-mm-zte-port-types.rules
Reading rules file: /lib/udev/rules.d/78-sound-card.rules
Reading rules file: /lib/udev/rules.d/80-debian-compat.rules
Reading rules file: /lib/udev/rules.d/80-drivers.rules
Reading rules file: /lib/udev/rules.d/80-ifupdown.rules
Reading rules file: /lib/udev/rules.d/80-iio-sensor-proxy.rules
Reading rules file: /lib/udev/rules.d/80-libinput-device-groups.rules
Reading rules file: /lib/udev/rules.d/80-mm-candidate.rules
Reading rules file: /lib/udev/rules.d/80-net-setup-link.rules
Reading rules file: /lib/udev/rules.d/80-udisks2.rules
Reading rules file: /lib/udev/rules.d/84-nm-drivers.rules
Reading rules file: /lib/udev/rules.d/85-hdparm.rules
Reading rules file: /lib/udev/rules.d/85-hwclock.rules
Reading rules file: /lib/udev/rules.d/85-nm-unmanaged.rules
Reading rules file: /lib/udev/rules.d/85-regulatory.rules
Reading rules file: /lib/udev/rules.d/90-alsa-restore.rules
Reading rules file: /lib/udev/rules.d/90-console-setup.rules
Reading rules file: /lib/udev/rules.d/90-libgpod.rules
Reading rules file: /lib/udev/rules.d/90-libinput-model-quirks.rules
Reading rules file: /lib/udev/rules.d/90-pulseaudio.rules
Reading rules file: /lib/udev/rules.d/95-cd-devices.rules
Reading rules file: /lib/udev/rules.d/95-upower-csr.rules
Reading rules file: /lib/udev/rules.d/95-upower-hid.rules
Reading rules file: /lib/udev/rules.d/95-upower-wup.rules
Reading rules file: /lib/udev/rules.d/97-hid2hci.rules
Reading rules file: /lib/udev/rules.d/99-systemd.rules
rules contain 393216 bytes tokens (32768 * 12 bytes), 34468 bytes strings
27397 strings (224884 bytes), 23795 de-duplicated (194019 bytes), 3603 trie nodes used
IMPORT builtin 'hwdb' /lib/udev/rules.d/50-udev-default.rules:15
RUN 'kmod load $env{MODALIAS}' /lib/udev/rules.d/80-drivers.rules:5
created db file '/run/udev/data/+pci:0000:00:19.0' for '/devices/pci0000:00/0000:00:19.0'
Unload module index
Unloaded link configuration context.
This program is for debugging only, it does not run any program
specified by a RUN key. It may show incorrect results, because
some values may be different, or not available at a simulation run.

ACTION=add
DEVPATH=/devices/pci0000:00/0000:00:19.0
DRIVER=e1000e
ID_MODEL_FROM_DATABASE=Ethernet Connection (2) I218-V
ID_PCI_CLASS_FROM_DATABASE=Network controller
ID_PCI_SUBCLASS_FROM_DATABASE=Ethernet controller
ID_VENDOR_FROM_DATABASE=Intel Corporation
MODALIAS=pci:v00008086d000015A1sv00001043sd000085C4bc02sc00i00
PCI_CLASS=20000
PCI_ID=8086:15A1
PCI_SLOT_NAME=0000:00:19.0
PCI_SUBSYS_ID=1043:85C4
SUBSYSTEM=pci
USEC_INITIALIZED=17414423
run: 'kmod load pci:v00008086d000015A1sv00001043sd000085C4bc02sc00i00'
=====
$> sudo udevadm info /sys/devices/pci0000:00/0000:00:19.0
=====
P: /devices/pci0000:00/0000:00:19.0
E: DEVPATH=/devices/pci0000:00/0000:00:19.0
E: DRIVER=e1000e
E: ID_MODEL_FROM_DATABASE=Ethernet Connection (2) I218-V
E: ID_PCI_CLASS_FROM_DATABASE=Network controller
E: ID_PCI_SUBCLASS_FROM_DATABASE=Ethernet controller
E: ID_VENDOR_FROM_DATABASE=Intel Corporation
E: MODALIAS=pci:v00008086d000015A1sv00001043sd000085C4bc02sc00i00
E: PCI_CLASS=20000
E: PCI_ID=8086:15A1
E: PCI_SLOT_NAME=0000:00:19.0
E: PCI_SUBSYS_ID=1043:85C4
E: SUBSYSTEM=pci
E: USEC_INITIALIZED=17414423

=====
$> sudo udevadm info -a /sys/devices/pci0000:00/0000:00:19.0
=====

Udevadm info starts with the device specified by the devpath and then
walks up the chain of parent devices. It prints for every device
found, all possible attributes in the udev rules key format.
A rule to match, can be composed by the attributes of the device
and the attributes from one single parent device.

  looking at device '/devices/pci0000:00/0000:00:19.0':
    KERNEL=="0000:00:19.0"
    SUBSYSTEM=="pci"
    DRIVER=="e1000e"
    ATTR{broken_parity_status}=="0"
    ATTR{class}=="0x020000"
    ATTR{consistent_dma_mask_bits}=="64"
    ATTR{d3cold_allowed}=="1"
    ATTR{device}=="0x15a1"
    ATTR{dma_mask_bits}=="64"
    ATTR{driver_override}=="(null)"
    ATTR{enable}=="1"
    ATTR{index}=="1"
    ATTR{irq}=="32"
    ATTR{label}==" Onboard LAN"
    ATTR{local_cpulist}=="0-11"
    ATTR{local_cpus}=="fff"
    ATTR{msi_bus}=="1"
    ATTR{numa_node}=="0"
    ATTR{subsystem_device}=="0x85c4"
    ATTR{subsystem_vendor}=="0x1043"
    ATTR{vendor}=="0x8086"

  looking at parent device '/devices/pci0000:00':
    KERNELS=="pci0000:00"
    SUBSYSTEMS==""
    DRIVERS==""

