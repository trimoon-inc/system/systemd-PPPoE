# Inspired by: https://blog.g3rt.nl/systemd-networkd-dhcpv6-pd-configuration.html

[Network]
# We only need a link-local address for IPv6 (required to run IPv6),
# but not for IPv4 (using DHCP).
LinkLocalAddressing=ipv6

# SLAAC IPv6 for obtaining the default route.
# This is needed, because the DHCPv6 response does not include an
# address with a gateway for the prefixes. We're supposed to use
# the SLAAC-announced default route one as the 'return route'.
IPv6AcceptRA=yes
# IPv6AcceptRA=no

# yes | [no] | prefer-public | kernel
IPv6PrivacyExtensions=prefer-public
# IPv6PrivacyExtensions=yes
# IPv6DuplicateAddressDetection=0
IPv6SendRA=yes
DHCPPrefixDelegation=yes

[IPv6AcceptRA]
# Similar for IPv6 via Router Advertisements; I'll handle DNS myself,
# please.
UseDNS=no
# Force starting the DHCPv6 client even if the Router Advertisement
# indicates it's not required.
DHCPv6Client=yes

# UseAutonomousPrefix=yes
# UseOnLinkPrefix=yes

[DHCPv6]
WithoutRA=solicit
# UseAddress=false

# Similar as for DHCPv4, I dislike systemd-networkd to use any other
# information in the reply.
UseHostname=no
UseDNS=no
UseNTP=no
# UseRoutes=no
# UseGateway=yes
# Use6RD=true

# https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-dhcpe/a3f91131-4eb3-4af3-937d-0de186d1b8ec
# 2.2.6.2 User Class Option Sent by DHCPv4 Server to DHCPv4 Client
# This will  request the user classes configured on the DHCPv4 server
# # OPTION_PARAMETER_REQUEST_LIST
# RequestOptions=55
# # OPTION_USER_CLASS
# SendOption=77

# UserClass=
# VendorClass=
